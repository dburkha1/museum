package edu.westga.museum.model;

/**
 * Class that handles all museum entrances.
 * 
 * @author danielburkhart
 *
 */
public class East implements Runnable {

	private boolean threadState;
	private int numberOutside;

	/**
	 * Initializes instance variables needed by the class.
	 */
	public East() {

		this.threadState = true;
		this.numberOutside = 0;
	}

	/**
	 * Getter method for the number of people outside the museum
	 * 
	 * @return the numberOutside The number of people waiting outside the museum
	 */
	public int getNumberOutside() {
		return this.numberOutside;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {

		while (this.threadState) {

			try {
				Thread.sleep((long) (Math.random() * 2000));
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			this.numberOutside++;

		}

	}

	/**
	 * Shows if anyone is currently waiting outside
	 * 
	 * @return True if someone is waiting outside, false otherwise.
	 */
	public boolean anyoneWaiting() {
		return (this.numberOutside > 0);
	}

	/**
	 * Admits entry into the museum, decreasing the number outside by 1.
	 */
	public void allowEntry() {

		if (this.anyoneWaiting()) {
			this.numberOutside--;
			System.out.println("Vistor enters");
		}

	}

	/**
	 * Stops the East thread from continuously running.
	 */
	public void stop() {
		this.threadState = false;
	}

}
