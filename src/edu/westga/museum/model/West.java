package edu.westga.museum.model;

/**
 * Class that handles all the exiting from the museum.
 * 
 * @author danielburkhart
 *
 */
public class West implements Runnable {

	private int numberOfPeopleInMuseum;
	private boolean threadState;

	/**
	 * Initializes the instance variables needed.
	 */
	public West() {

		this.numberOfPeopleInMuseum = 0;
		this.threadState = true;

	}

	/**
	 * Getter for the number of people in the museum
	 * 
	 * @return the numberOfPeopleInMuseum The number of people in the museum.
	 */
	public int getNumberOfPeopleInMuseum() {
		return numberOfPeopleInMuseum;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {

		while (this.threadState) {

			try {
				Thread.sleep((long) (Math.random() * 3000));
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			this.numberOfPeopleInMuseum--;

		}

	}

	/**
	 * Accepts a new person into the museum.
	 */
	public void acceptVisitor() {
		this.numberOfPeopleInMuseum++;
		System.out.println("Visitor leaves");
	}

	/**
	 * Stops the West thread.
	 */
	public void stop() {
		this.threadState = false;
	}

}
