package edu.westga.museum.model;

/**
 * Class that states whether the museum is open or not.
 * 
 * @author danielburkhart
 *
 */
public class Director implements Runnable {

	private boolean museumState;
	private boolean threadState;

	/**
	 * Initializes private instance variables needed by the class.
	 */
	public Director() {
		this.museumState = true;
		this.threadState = true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {

		while (this.threadState) {

			if (this.museumState == true) {

				System.out.println("Museum is open");

			} else if (this.museumState == false) {

				System.out.println("Museum is closed");

			}

			this.sleepForTenSeconds();

			this.museumState = !this.museumState;

		}

	}

	/**
	 * Causes thread to sleep for ten seconds.
	 */
	private void sleepForTenSeconds() {
		try {
			Thread.sleep(10 * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Public method to tell if the museum is open or not.
	 * 
	 * @return True if museum is open, false otherwise
	 */
	public boolean isOpen() {
		return this.museumState;
	}

	/**
	 * Stops the Director thread from continuously running.
	 */
	public void stop() {
		this.threadState = false;
	}

}
