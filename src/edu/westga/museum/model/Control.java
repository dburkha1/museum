/**
 * 
 */
package edu.westga.museum.model;

/**
 * Class that controls the entry and exit of the museum.
 * 
 * @author danielburkhart
 *
 */
public class Control implements Runnable {

	private boolean threadState;
	private Director director;
	private East east;
	private West west;

	/**
	 * Private constructor ensuring use of parameterized constructor
	 */
	private Control() {
		this.threadState = true;
	}

	/**
	 * Initializes the instance variables to the parameters of the constructor
	 */
	public Control(Director director, East east, West west) {

		this();

		if (director == null) {
			throw new IllegalArgumentException("Director is null");
		} else if (east == null) {
			throw new IllegalArgumentException("East is null");
		} else if (west == null) {
			throw new IllegalArgumentException("West is null");
		}

		this.director = director;
		this.east = east;
		this.west = west;

	}

	/**
	 * Run method of all threads.
	 */
	@Override
	public void run() {

		while (this.threadState) {

			try {
				Thread.sleep(5);
			} catch (InterruptedException e) {

				e.printStackTrace();
			}

			if (this.director.isOpen() && this.east.anyoneWaiting()) {
				this.east.allowEntry();
				this.west.acceptVisitor();
			}
		}

	}

	/**
	 * Stops the control thread.
	 */
	public void stop() {
		this.threadState = false;
	}

}
