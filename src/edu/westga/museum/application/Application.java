/**
 * 
 */
package edu.westga.museum.application;

import edu.westga.museum.model.Control;
import edu.westga.museum.model.Director;
import edu.westga.museum.model.East;
import edu.westga.museum.model.West;

/**
 * Application class that contains the main method.
 * 
 * @author danielburkhart
 *
 */
public class Application {

	/**
	 * The entry point of the program.
	 * 
	 * @param args
	 *            The arguments passed into the method
	 */
	public static void main(String[] args) {

		Director director = new Director();
		East east = new East();
		West west = new West();
		Control control = new Control(director, east, west);

		(new Thread(director)).start();
		(new Thread(east)).start();
		(new Thread(west)).start();
		(new Thread(control)).start();

		try {
			Thread.sleep(30 * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		director.stop();
		east.stop();
		west.stop();
		control.stop();
		
		System.out.println("\nNumber of people waiting: " + east.getNumberOutside());
		System.out.println("Number of people inside the museum: " + west.getNumberOfPeopleInMuseum());

	}
}
